//
//  MapViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate: class {
    func touchMapSaveButton(with location: Location)
}

class MapViewController: UIViewController {

    weak var delegate: MapViewControllerDelegate?
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let longPressRegognizer = UILongPressGestureRecognizer(target: self, action: #selector(MapViewController.longPress(longPressGestureRecognizer:)))
        longPressRegognizer.minimumPressDuration = 0.5
        view.addGestureRecognizer(longPressRegognizer)
    }
    
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == .began {
            let touchPoint = longPressGestureRecognizer.location(in: mapView)
            let touchMapCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = touchMapCoordinate
            mapView.removeAnnotations(mapView.annotations)
            mapView.addAnnotation(annotation)
        }
    }
    
    //MARK: Action
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        if let annotation = mapView.annotations.first {
            let location = Location()
            location.latitude = annotation.coordinate.latitude
            location.longitude = annotation.coordinate.longitude
            delegate?.touchMapSaveButton(with: location)
        }
        dismiss(animated: true, completion: nil)
    }
}
