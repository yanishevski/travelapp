//
//  AddPlaceViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

protocol AddPlaceViewControllerProtocol {
    func addPlaceSave(withTravel: Stay)
}

class AddStayViewController: UIViewController, MapViewControllerDelegate, SpendMoneyViewControllerDelegate {

    var delegate: AddPlaceViewControllerProtocol?
    @IBOutlet weak var nameOfCityTextField: UITextField!
    @IBOutlet weak var raitingLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var transportSegmentedControl: UISegmentedControl!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func raitingStepperCliked(_ sender: UIStepper) {
        raitingLabel.text = String(Int(sender.value))
    }
    
    @IBAction func saveButtonCliked(_ sender: UIButton) {
        delegate?.addPlaceSave(withTravel: getTravel())
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        nameOfCityTextField.resignFirstResponder()
        descriptionTextView.resignFirstResponder()
    }
    
    @IBAction func openMapViewControllerButtonCliked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapViewController.delegate = self
        present(mapViewController, animated: true, completion: nil)
    }
    
    func mapViewControllerButtonSaveClicked(with value: String) {
        locationLabel.text = value
    }
    
    @IBAction func openSpendMoneyViewControllerButtonCliked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let spendMoneyViewController = storyboard.instantiateViewController(withIdentifier: "SpendMoneyViewController") as! SpendMoneyViewController
        spendMoneyViewController.delegate = self
        present(spendMoneyViewController, animated: true, completion: nil)
    }
    
    func spendMoneyViewControllerButtunDoneCliked(with value: String) {
        sumLabel.text = value
    }
    
    func getTravel() -> Stay {
        let travel = Stay()
        if let city = nameOfCityTextField.text {
            travel.city = city
        }
        if let raiting = raitingLabel.text {
            travel.rating = raiting
        }
        if let location = locationLabel.text {
            travel.location = location
        }
        if let sum = sumLabel.text {
            travel.sum = sum
        }
        if let transoprt = transportSegmentedControl.titleForSegment(at: transportSegmentedControl.selectedSegmentIndex) {
            travel.transport = transoprt
        }
        if let description = descriptionTextView.text {
            travel.description = description
        }
        return travel
    }
}
