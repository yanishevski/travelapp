//
//  AddPlaceViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

protocol AddStayViewControllerDelegate: class {
    func touchAddStayButton(with stay: Stay)
}

class AddStayViewController: UIViewController {

    weak var delegate: AddStayViewControllerDelegate?
    let listTransports = [Transport.plane, Transport.train, Transport.car]
    @IBOutlet weak var nameOfCityTextField: UITextField!
    @IBOutlet weak var raitingLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var transportSegmentedControl: UISegmentedControl!
    @IBOutlet weak var descriptionTextView: UITextView!
    let stay = Stay()
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.isNavigationBarHidden = true
    }
    //MARK:
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //MARK: Action
    @IBAction func raitingStepperCliked(_ sender: UIStepper) {
        raitingLabel.text = String(Int(sender.value))
    }
    
    @IBAction func saveButtonCliked(_ sender: UIButton) {
        configStay()
        delegate?.touchAddStayButton(with: stay)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openMapViewControllerButtonCliked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapViewController.delegate = self
        present(mapViewController, animated: true, completion: nil)
    }
    
    @IBAction func openSpendMoneyViewControllerButtonCliked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let spendMoneyViewController = storyboard.instantiateViewController(withIdentifier: "SpendMoneyViewController") as! SpendMoneyViewController
        spendMoneyViewController.delegate = self
        present(spendMoneyViewController, animated: true, completion: nil)
    }
    
    //MARK: Private
    private func configStay() {
        
        if let city = nameOfCityTextField.text {
            stay.city = city
        }
        if let raiting = raitingLabel.text {
            stay.rating = raiting
        }
        if let sum = sumLabel.text {
            stay.sum = sum
        }
        stay.transport = listTransports[transportSegmentedControl.selectedSegmentIndex]
        if let description = descriptionTextView.text {
            stay.stayDescription = description
        }
    }
}

extension AddStayViewController: MapViewControllerDelegate, SpendMoneyViewControllerDelegate {
    //MARK: MapViewControllerDelegate method
    func touchMapSaveButton(with location: Location) {
        locationLabel.text = ("\(location.latitude), \(location.longitude)")
        stay.location = location
    }
    
    //MARK: SpendMoneyViewControllerDelegate method
    func touchSpendMoneyDoneButtunon(with value: String) {
        sumLabel.text = value
    }
}
