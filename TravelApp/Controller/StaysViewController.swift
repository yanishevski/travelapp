//
//  PlacesViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import RealmSwift

class StaysViewController: UIViewController {
   
    var trip: Trip = Trip()
    @IBOutlet weak var staysTableView: UITableView!
    @IBOutlet weak var notStaysLabel: UILabel!
    private let searchController = UISearchController(searchResultsController: nil)
    private var filteredStays = [Stay]()
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return true }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
        setupLongPressGestureRecognizer()
        if let country = trip.country {
            navigationItem.title = country
        }
    }
    
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == .began {
            let touchPoint = longPressGestureRecognizer.location(in: staysTableView)
            if let indexPath = staysTableView.indexPathForRow(at: touchPoint) {
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                let delete = UIAlertAction(title: "Удалить", style: .destructive) { (_) in
                    if self.isFiltering {
                        DatabaseManager.instance.removeStayFromDatabase(self.filteredStays[indexPath.row])
                    } else {
                        DatabaseManager.instance.removeStayFromDatabase(self.trip.stays[indexPath.row])
                    }
                    self.staysTableView.reloadData()
                }
                let cancel = UIAlertAction(title: "Отмена", style:  .cancel, handler: nil)
                alertController.addAction(cancel)
                alertController.addAction(delete)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Action
    @IBAction func backButtonClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func addPlaceButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addPlaceViewController = storyboard.instantiateViewController(withIdentifier: "AddPlaceViewController") as! AddStayViewController
        addPlaceViewController.delegate = self
        present(addPlaceViewController, animated: false, completion: nil)
    }
    
    //MARK: Configuration Hidden
    private func configHidden() {
        if trip.stays.isEmpty {
            staysTableView.isHidden = true
            notStaysLabel.isHidden = false
        } else {
            staysTableView.isHidden = false
            notStaysLabel.isHidden = true
        }
    }
    
    //MARK: Setup the SearchController
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск остановки"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    //MARK: Setup the LongPressGestureRecognizer
    private func setupLongPressGestureRecognizer() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(longPressGestureRecognizer:)))
        longPressRecognizer.minimumPressDuration = 1.0
        view.addGestureRecognizer(longPressRecognizer)
    }
}

extension StaysViewController: AddStayViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stayCell", for: indexPath) as! StayCell
        if isFiltering {
            cell.setup(with: filteredStays[indexPath.row])
        } else {
            cell.setup(with: trip.stays[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configHidden()
        if isFiltering {
            return filteredStays.count
        }
        return trip.stays.count
    }
    
    //MARK: AddStayViewControllerDelegate
    func touchAddStayButton(with stay: Stay) {
        DatabaseManager.instance.updateTrip(trip, with: stay)
        staysTableView.reloadData()
    }
}

extension StaysViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        staysTableView.reloadData()
    }
    
    private func filterContentForSearchText(_ text: String) {
        filteredStays = trip.stays.filter { $0.city?.lowercased().contains(text.lowercased()) ?? false }
    }
}
