//
//  SpendMoneyViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/28/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

protocol SpendMoneyViewControllerDelegate: class {
    func touchSpendMoneyDoneButtunon(with value: String)
}

class SpendMoneyViewController: UIViewController {

    weak var delegate: SpendMoneyViewControllerDelegate?
    @IBOutlet weak var sumTextField: UITextField!
    @IBOutlet weak var currencySegmentedControl: UISegmentedControl!
    @IBOutlet weak var doneButton: UIButton!
    private let currencies = [Currency.dollar, Currency.euro, Currency.ruble]
    private lazy var currency = currencies.first
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    //MARK:
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //MARK: Action
    @IBAction func doneButtonCliked(_ sender: UIButton) {
        delegate?.touchSpendMoneyDoneButtunon(with: getSum())
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func currencySegmentedControlTapped(_ sender: UISegmentedControl) {
        let newCurrency = currencies[currencySegmentedControl.selectedSegmentIndex]
        if let currency = currency {
            CurrencyConverter.instance.getCoefficientOfCurrencies(for: (firstCurrency: currency, secondCurrency: newCurrency)) { (coefficient) in
                if let sumString = self.sumTextField.text {
                    if let sum = Double(sumString) {
                        self.sumTextField.text = String(round(100 * sum * coefficient)/100)
                    }
                }
            }
            self.currency = newCurrency
        }
    }
    
    //MARK: Private
    private func configUI() {
        //sumTextField
        sumTextField.layer.cornerRadius = 6
        //doneButton
        doneButton.layer.cornerRadius = 8
        //currencySegmentedControl
        currencySegmentedControl.removeAllSegments()
        for index in currencies.indices {
            currencySegmentedControl.insertSegment(withTitle: currencies[index].sign(), at: index, animated: false)
        }
        currencySegmentedControl.selectedSegmentIndex = 0
    }
    
    private func configCurrencySegmentedControl() {
        
    }
    
    private func getSum() -> String {
        var result = ""
        if let currency = currencySegmentedControl.titleForSegment(at: currencySegmentedControl.selectedSegmentIndex) {
            result += currency
            if let sum = sumTextField.text {
                result += sum
            }
        }
        return result
    }
}
