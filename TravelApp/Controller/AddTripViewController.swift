//
//  AddTripViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

protocol AddTripViewControllerDelegate: class {
    func touchAddTripSaveButton(with trip: Trip)
}

class AddTripViewController: UIViewController {

    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    weak var delegate: AddTripViewControllerDelegate?
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK:
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    //MARK: Action
    @IBAction func touchAddButton(_ sender: UIButton) {
        if let country = countryTextField.text {
            let trip = Trip()
            trip.country = country
            if let description = descriptionTextField.text {
                trip.tripDescription = description
            }
            delegate?.touchAddTripSaveButton(with: trip)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func touchCancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
