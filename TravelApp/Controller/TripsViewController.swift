//
//  TripsViewController.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit
import RealmSwift

class TripsViewController: UIViewController {

    private var trips = [Trip]()
    private var filteredTrips = [Trip]()
    @IBOutlet weak var tripTableView: UITableView!
    @IBOutlet weak var notTripsLabel: UILabel!
    private let searchController = UISearchController(searchResultsController: nil)
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tripTableView.delegate = self
        tripTableView.dataSource = self
        setupSearchController()
        setupLongPressRecognizer()
        if trips.isEmpty {
            DatabaseManager.instance.getTripsFromDatabase { (trips) in
                if !trips.isEmpty {
                    self.trips = trips
                }
            }
            tripTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tripTableView.reloadData()
    }
    
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == .began {
            let touchPoint = longPressGestureRecognizer.location(in: tripTableView)
            if let indexPath = tripTableView.indexPathForRow(at: touchPoint) {
                let alertController = UIAlertController(title: "Удаление путешествия", message: nil, preferredStyle: .actionSheet)
                let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
                let delete = UIAlertAction(title: "Удалить", style: .destructive) { (_) in
                    if self.isFiltering {
                        let trip = self.filteredTrips.remove(at: indexPath.row)
                        self.trips = self.trips.filter { $0 != trip }
                        DatabaseManager.instance.removeTripFromDatabase(trip)
                    } else {
                        DatabaseManager.instance.removeTripFromDatabase(self.trips.remove(at: indexPath.row))
                    }
                    self.tripTableView.reloadData()
                }
                alertController.addAction(delete)
                alertController.addAction(cancel)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Action
    @IBAction func touchAddTripButton(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let addTripViewController = storyboard.instantiateViewController(withIdentifier: "addTripViewController") as! AddTripViewController
        addTripViewController.delegate = self
        present(addTripViewController, animated: true)
    }
    
    //MARK: Hiding Configuration
    private func configHidden() {
        if trips.isEmpty {
            tripTableView.isHidden = true
            notTripsLabel.isHidden = false
        } else {
            tripTableView.isHidden = false
            notTripsLabel.isHidden = true
        }
    }
    
    //MARK: Setup the Search Controller
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск путешествия"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    //MARK: Setup the LongPressGestureRecognizer
    private func setupLongPressRecognizer() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(longPressGestureRecognizer:)))
        longPressRecognizer.minimumPressDuration = 1.0
        self.view.addGestureRecognizer(longPressRecognizer)
    }
}

extension TripsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configHidden()
        if isFiltering {
            return filteredTrips.count
        }
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tripCell") as! TripCell
        if isFiltering {
            cell.setup(with: filteredTrips[indexPath.row])
        } else {
            cell.setup(with: trips[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let staysViewController = storyboard.instantiateViewController(withIdentifier: "staysViewController") as? StaysViewController {
            if isFiltering {
                staysViewController.trip = filteredTrips[indexPath.row]
            } else {
                staysViewController.trip = trips[indexPath.row]
            }
            if let navigation = navigationController {
                navigation.pushViewController(staysViewController, animated: false)
            }
        }
    }
}

extension TripsViewController: AddTripViewControllerDelegate {
    
    func touchAddTripSaveButton(with trip: Trip) {
        trips.append(trip)
        DatabaseManager.instance.saveTripToDatabase(trip)
        tripTableView.reloadData()
    }
}

extension TripsViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        tripTableView.reloadData()
    }
    
    private func filterContentForSearchText(_ text: String) {
        filteredTrips = trips.filter { $0.country?.lowercased().contains(text.lowercased()) ?? false}
    }
}
