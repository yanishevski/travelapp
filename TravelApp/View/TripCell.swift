//
//  TripCell.swift
//  TravelApp
//
//  Created by Артем Хохлов on 12/19/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {

    @IBOutlet weak var tripCellView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet var ratingImageViewCollection: [UIImageView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tripCellView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setup(with trip: Trip) {
        if let country = trip.country {
            countryLabel.text = country
        }
        if let description = trip.tripDescription {
            descriptionLabel.text = description
        }
        for index in 0..<trip.rating {
            ratingImageViewCollection[index].image = UIImage.init(named: "icon_star_gold")
        }
    }
}
