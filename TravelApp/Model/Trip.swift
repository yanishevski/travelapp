//
//  Trip.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import RealmSwift

class Trip: Object {
    @objc dynamic var tripId = UUID().uuidString
    @objc dynamic var country: String?
    @objc dynamic var tripDescription: String?
    var rating: Int {
        var sumOfRatings = 0
        for stay in stays {
            if let ratingStay = stay.rating {
                if let rating = Int(ratingStay) {
                    sumOfRatings += rating
                }
            }
        }
        if sumOfRatings != 0 {
            return sumOfRatings / stays.count
        } else {
            return 0
        }
    }
    let stays = List<Stay>()
    
    override static func primaryKey() -> String? {
        return "tripId"
    }
}
