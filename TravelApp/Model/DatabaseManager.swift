//
//  DatabaseManager.swift
//  TravelApp
//
//  Created by Артем Хохлов on 12/9/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let instance = DatabaseManager()
    
    func getTripsFromDatabase(onComplite: ([Trip]) -> Void) {
        let realm = try! Realm()
        let trips = Array(realm.objects(Trip.self))
        onComplite(trips)
    }
    
    func saveTripsToDatabase(_ trips: [Trip]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(trips, update: true)
        }
        
    }
    
    func updateTrip(_ trip: Trip, with stay: Stay) {
        let realm = try! Realm()
        try! realm.write {
            trip.stays.append(stay)
        }
    }
    
    func saveTripToDatabase(_ trip: Trip) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(trip, update: true)
        }
    }
    
    func removeTripFromDatabase(_ trip: Trip) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(trip)
        }
    }
    
    func removeStayFromDatabase(_ stay: Stay) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(stay)
        }
    }
}
