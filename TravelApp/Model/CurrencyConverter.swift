//
//  CurrencyConverter.swift
//  TravelApp
//
//  Created by Артем Хохлов on 11/25/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import Alamofire

class CurrencyConverter {
    
    static let instance = CurrencyConverter()
    
    private enum Constants {
        static let baseUrl = "https://free.currencyconverterapi.com/api/v6"
    }
    
    private enum EndPoint {
        static let convert = "/convert"
    }
    
    func getCoefficientOfCurrencies(for currencies: (firstCurrency: Currency, secondCurrency: Currency), onCompletion: ((Double) -> Void)?) {
        let stringUrl = Constants.baseUrl + EndPoint.convert
        let url = URL.init(string: stringUrl)!
        let stringCurrencies = currencies.firstCurrency.rawValue + "_" + currencies.secondCurrency.rawValue
        let parameters = ["q": stringCurrencies,
                          "compact": "ultra"]
        request(url, method: .get, parameters: parameters).responseJSON { (response) in
            if let currency = response.result.value as? [String: Double] {
                if let coefficient = currency[stringCurrencies] {
                    onCompletion?(coefficient)
                }
            }
        }
    }
}
