//
//  Place.swift
//  TravelApp
//
//  Created by Артем Хохлов on 10/29/18.
//  Copyright © 2018 Artem Hohlov. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift

class Location: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
    var coordinate: CLLocationCoordinate2D {
        get {
        return CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude)
        }
        set {
            latitude = newValue.latitude
            longitude = newValue.longitude
        }
    }
    
}
